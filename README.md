# popl seminaariesitys: d-lang

Repon rakenne:

```
./
	.editorconfig        -- onhan sinulla editori joka tukee tätä? :)
	.gitignore           -- lisää editorisi jättämät romppeet tänne
	VagrantFile          -- lisätietoja projektin Wikissä
	provision.sh         -- vagrantfilen ajama "asenna kaikki"
	src/                 -- tänne tuotettu koodi
		0001-hello-world -- hyvin simppeli hello world
```

[Wiki](https://bitbucket.org/koivunej/principles_of_prog_lang/wiki/) sisältää Vagrantin käytöstä pikaohjeen.
