#!/bin/bash

set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

[ -f "$DIR/source/app.d" ] || exit 1
cd "$DIR" && rm -f app.o app && gdc -g source/app.d -o app.gdc && ./app.gdc
