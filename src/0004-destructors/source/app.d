import std.stdio;

class Destrc {
	int a;

	this(int a) {
		this.a = a;
	}

	void car() {
		writefln("%d car: %s", a, this);
	}

	~this() {
		writefln("%d destructor", a);
	}
}

void main() {
	Destrc a = new Destrc(1);
	foo();
	a.car();
}

void foo() {
	Destrc a = new Destrc(2);
	bar();
	a.car();
}

void bar() {
	Destrc a = new Destrc(3);
	a.car();
}
