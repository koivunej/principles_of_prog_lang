#!/bin/bash

set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

[ -f "$DIR/app.d" ] || exit 1
cd "$DIR" && rm -f app.o app && dmd -unittest -g app.d && ./app && echo ok.
