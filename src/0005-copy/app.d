struct Foo {
	int x;

	@property Foo bar() const {
		return this;
	}
}

unittest {
	Foo foo = Foo(5);
	Foo tmp = foo.bar;

	assert(foo == tmp);
	assert(&foo != &tmp);
}

void main() {

}
