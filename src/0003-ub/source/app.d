import std.stdio;

int main() {
	char[] s = "foobar".dup;

	immutable(char)[] p = cast(immutable)s; // @safe estää UB tässä

	writefln("s = %s, p = %s", s, p);

	s[0] = 'F';

	writefln("s = %s, p = %s", s, p);

	return 0;
}
