import std.stdio;
import std.typecons : Nullable;
import std.string;
import std.conv;

struct InputRow {
	string shop;
	string location;
	string name;
	Nullable!double availability;
}

Nullable!InputRow parse_input(string text) {
	if (text == null || text == "") {
		return (Nullable!InputRow).init;
	}

	auto parts = split(text, ";");

	auto availability = (Nullable!double).init;

	if (parts[3] != "loppu") {
		try {
			availability = to!double(parts[3]);
		} catch (ConvException e) {
			return (Nullable!InputRow).init;
		}
	}

	InputRow r = InputRow(parts[0], parts[1], parts[2], availability);
	Nullable!InputRow ret = r;
	return ret;
}

unittest {
	assert(parse_input(null).isNull);
	assert(parse_input("").isNull);
	Nullable!InputRow val = InputRow("Foobar", "Abbacd", "mustikka", Nullable!double(3.2));
	assert(parse_input("Foobar;Abbacd;mustikka;3.2") == val);
	assert(parse_input("Foobar;Abbacd;mustikka;asdsadf").isNull);

	auto parsed = parse_input("Foobar;Abbacd;mustikka;loppu");

	assert(parsed.shop == "Foobar");
	assert(parsed.location == "Abbacd");
	assert(parsed.name == "mustikka");
	assert(parsed.availability.isNull);
}

int main() {

	Nullable!double[string][string][string] db;

	File f = File("tuotetiedot.txt", "r");
	while (!f.eof()) {
		Nullable!InputRow r = parse_input(strip(f.readln()));
		if (r.isNull) {
			continue;
		}

		db[r.shop][r.location][r.name] = r.availability;
	}

	foreach (shop; db.keys) {
		writefln("%s", shop);
		foreach (location; db[shop].keys) {
			writefln("  %s", location);
			foreach (name; db[shop][location].keys) {
				auto price = db[shop][location][name];
				if (price.isNull) {
					writefln("    %s LOPPU", name);
				} else {
					writefln("    %s %.2f", name, price);
				}
			}
		}
	}

	while (!stdin.eof()) {
		stdout.write("tuotehaku> ");
		auto cmd = split(strip(stdin.readln()), " ");

		if (cmd.length == 0) {
			continue;
		}

		switch (cmd[0]) {
			case "lopeta": { return 0; }
			case "kauppaketjut": {
				if (cmd.length != 1) { break; }
				foreach (k; db.keys) {
					writeln(k);
				}
				break;
			}
			case "myymalat": {
				if (cmd.length != 2) { break; }
				auto target_shop = cmd[1];
				foreach (loc; db[target_shop].keys) {
					writeln(loc);
				}
				break;
			}
			case "halvin": {
				if (cmd.length != 2) { break; }
				auto target_name = cmd[1];
				break;
			}
			case "tuotevalikoima": {
				if (cmd.length != 3) { break; }
				auto target_shop = cmd[1];
				auto target_location = cmd[2];
				break;
			}
			case "tuotenimet": {
				if (cmd.length != 1) { break; }
				break;
			}
			default: break;
		}
	}

	writeln();

	return 0;
}
