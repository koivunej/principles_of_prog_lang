#!/bin/bash
#
# build using dub and dmd
set -eu

dub build -q

out="$(./hello-world)"

if [ "$out" != 'Hello, world!' ]; then
	echo "Unexpected output:" >&2
	echo "$out" >&2
else
	echo "hello-world: ok" >&2
fi
