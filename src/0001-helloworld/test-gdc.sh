#!/bin/bash
#
# build using gdc
set -eu

gdc source/app.d -o hello-world

out="$(./hello-world)"

if [[ "$out" != 'Hello, world!' ]]; then
	echo "Unexpected output:" >&2
	echo "$out" >&2
else
	echo "hello-world: ok" >&2
fi
