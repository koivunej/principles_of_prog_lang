Hello world esimerkki ja pari "testrunneria".

`./test-gdc.sh` toiminee suoraan, mutta `./test-dmd.sh` edellyttää, että olet aktivoinut `~/dlang` alle asennetun dmd ympäristön, eli suorittanut `source ~/dlang/dmd-2.071.2/activate` nykyisessä shellissäsi.
