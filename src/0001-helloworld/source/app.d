/*
https://en.wikibooks.org/wiki/D_(The_Programming_Language)/d2/Hello,_World!
*/

import std.stdio;

void main()
{
	writeln("Hello, world!");
}
